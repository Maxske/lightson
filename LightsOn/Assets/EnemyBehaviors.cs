﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviors : MonoBehaviour {

	int floorMask;
	NavMeshAgent nav;
	float camRayLength = 100f;
	Transform player;
	Transform triggerPlatform;


	// Update is called once per frame
	void Awake() {
		floorMask = LayerMask.GetMask ("Floor");
		nav = GetComponent <NavMeshAgent> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		triggerPlatform = GameObject.FindGameObjectWithTag ("DOORPLATFORM").transform;

	}
	void Update () {
		if (GameObject.FindGameObjectWithTag("FLASHLIGHT").GetComponent<Light>().enabled == true) {
			// Create a ray from the mouse cursor on screen in the direction of the camera.
			Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);

			// Create a RaycastHit variable to store information about what was hit by the ray.
			RaycastHit floorHit;

			// Perform the raycast and if it hits something on the floor layer...
			if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask)) {
			
				nav.SetDestination (floorHit.point);
			}

		}

		if (GameObject.FindGameObjectWithTag("CONTROLLIGHT").GetComponent<Light>().enabled == true) {
			nav.SetDestination (triggerPlatform.position);

		}
			
	}
}
