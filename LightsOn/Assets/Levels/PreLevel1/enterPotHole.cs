﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class enterPotHole : MonoBehaviour {
	public GameObject e;
	// Use this for initialization
	void Start () {
		e.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == ("POTHOLEENTRANCE")) 
		{
			e.SetActive (true);
			if (Input.GetKeyDown ("e")) 
			{
				SceneManager.LoadScene ("PipeArea");

			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == ("POTHOLEENTRANCE")) 
		{


			e.SetActive (false);

		}
	}
}
