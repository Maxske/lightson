﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {


	GameObject playButton;
	GameObject settingsButton;
	GameObject aboutButton;
	GameObject quitButton;

	GameObject resolutionButton;
	GameObject graphicsButton;
	GameObject backButton;

	GameObject highButton;
	GameObject mediumButton;
	GameObject lowButton;
	GameObject backGraButton;

	GameObject fullscreenButton;
	GameObject windowedButton;
	GameObject backResButton;


	// Use this for initialization
	void Start () {

		Screen.SetResolution (1280, 800, false);



		playButton = GameObject.FindGameObjectWithTag ("PLAYBUTTON");
		settingsButton = GameObject.FindGameObjectWithTag ("SETTINGSBUTTON");
		aboutButton = GameObject.FindGameObjectWithTag ("ABOUTBUTTON");
		quitButton = GameObject.FindGameObjectWithTag ("QUITBUTTON");
		resolutionButton = GameObject.FindGameObjectWithTag ("RESOLUTIONBUTTON");
		graphicsButton = GameObject.FindGameObjectWithTag ("GRAPHICSBUTTON");
		backButton = GameObject.FindGameObjectWithTag ("BACKBUTTON");

		highButton = GameObject.FindGameObjectWithTag ("HIGHBUTTON");
		mediumButton = GameObject.FindGameObjectWithTag ("MEDIUMBUTTON");
		lowButton = GameObject.FindGameObjectWithTag ("LOWBUTTON");
		backGraButton = GameObject.FindGameObjectWithTag ("GRAPHICSBACKBUTTON");

		windowedButton = GameObject.FindGameObjectWithTag ("WINDOWEDBUTTON");
		fullscreenButton = GameObject.FindGameObjectWithTag ("FULLSCREENBUTTON");
		backResButton = GameObject.FindGameObjectWithTag ("RESBACKBUTTON");

		resolutionButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
		graphicsButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
		backButton.GetComponentInChildren<MeshRenderer> ().enabled = false;

		highButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
		mediumButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
		lowButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
		backGraButton.GetComponentInChildren<MeshRenderer> ().enabled = false;

		fullscreenButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
		windowedButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
		backResButton.GetComponentInChildren<MeshRenderer> ().enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter()
	{
		gameObject.GetComponent<Renderer> ().material.color = Color.red;
	}

	void OnMouseExit()
	{
		gameObject.GetComponent<Renderer> ().material.color = Color.white;
	}

	void OnMouseUp()
	{
		if(gameObject.tag == "PLAYBUTTON")
			SceneManager.LoadScene("PreLevel1");
		if(gameObject.tag == "QUITBUTTON")
			Application.Quit ();
		if (gameObject.tag == "SETTINGSBUTTON") 
		{
			resolutionButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			resolutionButton.AddComponent<BoxCollider> ();
			graphicsButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			graphicsButton.AddComponent<BoxCollider> ();
			backButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			backButton.AddComponent<BoxCollider> ();

			playButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			aboutButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			settingsButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			quitButton.GetComponentInChildren<MeshRenderer> ().enabled = false;

			Destroy (playButton.GetComponent<BoxCollider> ());
			Destroy (settingsButton.GetComponent<BoxCollider> ());
			Destroy (aboutButton.GetComponent<BoxCollider> ());
			Destroy (quitButton.GetComponent<BoxCollider> ());

		}

		if (gameObject.tag == "BACKBUTTON") 
		{
			playButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			playButton.AddComponent<BoxCollider> ();
			settingsButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			settingsButton.AddComponent<BoxCollider> ();
			aboutButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			aboutButton.AddComponent<BoxCollider> ();
			quitButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			quitButton.AddComponent<BoxCollider> ();

			resolutionButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			graphicsButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			backButton.GetComponentInChildren<MeshRenderer> ().enabled = false;


			Destroy (resolutionButton.GetComponent<BoxCollider> ());
			Destroy (graphicsButton.GetComponent<BoxCollider> ());
			Destroy (backButton.GetComponent<BoxCollider> ());

		}

		if (gameObject.tag == "GRAPHICSBUTTON") 
		{
			highButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			highButton.AddComponent<BoxCollider> ();
			mediumButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			mediumButton.AddComponent<BoxCollider> ();
			lowButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			lowButton.AddComponent<BoxCollider> ();
			backGraButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			backGraButton.AddComponent<BoxCollider> ();

			resolutionButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			graphicsButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			backButton.GetComponentInChildren<MeshRenderer> ().enabled = false;


			Destroy (resolutionButton.GetComponent<BoxCollider> ());
			Destroy (graphicsButton.GetComponent<BoxCollider> ());
			Destroy (backButton.GetComponent<BoxCollider> ());

		}

		if (gameObject.tag == "GRAPHICSBACKBUTTON") 
		{
			resolutionButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			resolutionButton.AddComponent<BoxCollider> ();
			graphicsButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			graphicsButton.AddComponent<BoxCollider> ();
			backButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			backButton.AddComponent<BoxCollider> ();

			highButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			mediumButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			lowButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			backGraButton.GetComponentInChildren<MeshRenderer> ().enabled = false;

			Destroy (highButton.GetComponent<BoxCollider> ());
			Destroy (mediumButton.GetComponent<BoxCollider> ());
			Destroy (lowButton.GetComponent<BoxCollider> ());
			Destroy (backGraButton.GetComponent<BoxCollider> ());
		}

		if (gameObject.tag == "HIGHBUTTON")
			QualitySettings.SetQualityLevel (5);
		if (gameObject.tag == "MEDIUMBUTTON")
			QualitySettings.SetQualityLevel (3);
		if (gameObject.tag == "LOWBUTTON")
			QualitySettings.SetQualityLevel (2);

		if (gameObject.tag == "RESOLUTIONBUTTON") 
		{
			windowedButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			windowedButton.AddComponent<BoxCollider> ();
			fullscreenButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			fullscreenButton.AddComponent<BoxCollider> ();
			backResButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			backResButton.AddComponent<BoxCollider> ();

			resolutionButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			graphicsButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			backButton.GetComponentInChildren<MeshRenderer> ().enabled = false;


			Destroy (resolutionButton.GetComponent<BoxCollider> ());
			Destroy (graphicsButton.GetComponent<BoxCollider> ());
			Destroy (backButton.GetComponent<BoxCollider> ());
		}

		if (gameObject.tag == "RESBACKBUTTON")
		{
			resolutionButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			resolutionButton.AddComponent<BoxCollider> ();
			graphicsButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			graphicsButton.AddComponent<BoxCollider> ();
			backButton.GetComponentInChildren<MeshRenderer> ().enabled = true;
			backButton.AddComponent<BoxCollider> ();


			fullscreenButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			windowedButton.GetComponentInChildren<MeshRenderer> ().enabled = false;
			backResButton.GetComponentInChildren<MeshRenderer> ().enabled = false;


			Destroy (fullscreenButton.GetComponent<BoxCollider> ());
			Destroy (windowedButton.GetComponent<BoxCollider> ());
			Destroy (backResButton.GetComponent<BoxCollider> ());
		}

		if(gameObject.tag == "WINDOWEDBUTTON")
			Screen.SetResolution (1280, 800, false);
		if(gameObject.tag == "FULLSCREENBUTTON")
			Screen.SetResolution (1920, 1080, true);



	}
		
}
