﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class levelControlls : MonoBehaviour {
	public GameObject text,panel,e, leverUp, leverBot, wheel,water,ladder;
	public GameObject [] lights;
	// Use this for initialization

	public bool hasLever;
	public bool gotTask;
	public bool drained;
	public bool drainCompleted;
	public bool rotate;
	public bool ready;

	void Start () {
		text.SetActive(false);
		panel.SetActive (false);
		e.SetActive (false);

		rotate = false;
		drained = false;
		hasLever = false;
		gotTask = false;
		drainCompleted = false;
		ready = false;

	}

	// Update is called once per frame
	void Update () {
		if (rotate == true) {
			wheel.transform.Rotate(new Vector3(0,Time.deltaTime * 10,0));
			if(wheel.transform.rotation.x > 180)
					rotate = false;
		}
		if (drained == true) {
			water.transform.Translate(new Vector3(0,Time.deltaTime * -1,0));
			if (water.transform.position.y < 1.1) {
				drained = false;
				drainCompleted = true;
			}
		}


	
	}
	void OnTriggerEnter(Collider other)
	{
		
	}

	void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == ("PIPEBASE")) 
		{
			if (hasLever == false) {
				panel.SetActive (true);
				text.SetActive (true);
				gotTask = true;
			} else {
				e.SetActive (true);
				if (Input.GetKeyDown ("e")) 
				{
					ready = true;
					leverUp.GetComponent<MeshRenderer> ().enabled = true;
					ladder.GetComponent<Rigidbody> ().isKinematic = false;
					ladder.GetComponent<Rigidbody> ().useGravity = true;
				}
			}


		}
		if (other.gameObject.tag == ("PIPEWHEEL") && gotTask == true&& drainCompleted == false) 
		{
			e.SetActive (true);
			if (Input.GetKeyDown ("e")) 
			{
				drained = true;
				rotate = true;
			}

		}
		if (other.gameObject.tag == ("PIPELEVER") && drainCompleted == true && hasLever == false) {
			e.SetActive (true);
			if (Input.GetKeyDown ("e")) 
			{
				hasLever = true;
				Destroy (other.gameObject);
				e.SetActive (false);

				for (int i = 0; i < 14; i++) {
					lights [i].GetComponent<Light> ().color = Color.red;
				}
				//leverUp.transform.Rotate (10, 10, 10);
			}
		}
		if (other.gameObject.tag == ("EXITLADDER") && ready == true) {
			e.SetActive (true);
			if (Input.GetKeyDown ("e")) 
			{
				SceneManager.LoadScene ("Level1");

			}
		}


	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == ("PIPEBASE")) 
		{
			text.SetActive (false);
			panel.SetActive (false);

			e.SetActive (false);
		}
		if (other.gameObject.tag == ("PIPEWHEEL")) {
			e.SetActive (false);
		}
		if (other.gameObject.tag == ("PIPELEVER")) {
			e.SetActive (false);
		}
		if (other.gameObject.tag == ("EXITLADDER") && ready == true) {
			e.SetActive (false);

		}
	}




}

