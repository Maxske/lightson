﻿using UnityEngine;
using System.Collections;

public class Flashlight : MonoBehaviour {
	void Awake (){
		GetComponent<Light> ().enabled = false;
	}
			
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown(KeyCode.Mouse0)) {
			if (GetComponent<Light> ().enabled == true)
				GetComponent<Light> ().enabled = false;
			else
				GetComponent<Light> ().enabled = true;
		}
					
	}
}
